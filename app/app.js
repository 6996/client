'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ui.bootstrap',
  'ui.router',
    'ngMaterial',
    'ngAnimate',
    'toastr',
    'ngSanitize',
    'satellizer',
    'ngIdle',
  'myApp.version'

]).config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]).config(['KeepaliveProvider', 'IdleProvider', function(KeepaliveProvider, IdleProvider) {
    IdleProvider.idle(100);
    IdleProvider.timeout(100);
    KeepaliveProvider.interval(10);
}])
    .config(function($stateProvider, $urlRouterProvider, $authProvider) {
        var skipIfLoggedIn = ['$q', '$auth', function($q, $auth) {
            var deferred = $q.defer();
            if ($auth.isAuthenticated()) {
                deferred.reject();
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }];

        var loginRequired = ['$q', '$location', '$auth', function($q, $location, $auth) {
            var deferred = $q.defer();
            if ($auth.isAuthenticated()) {
                deferred.resolve();
            } else {
                $location.path('/login');
            }
            return deferred.promise;
        }];

        $stateProvider
        // route to show our basic form (/form)
            .state('home',{
                url: '/',
                templateUrl: 'home.html'

            })
            .state('login', {
                url: '/login',
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl',
                resolve: {
                    skipIfLoggedIn: skipIfLoggedIn
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'views/signup.html',
                controller: 'SignupCtrl',
                resolve: {
                    skipIfLoggedIn: skipIfLoggedIn
                }
            })
            .state('logout', {
                url: '/logout',
                template: null,
                controller: 'LogoutCtrl'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'views/profile.html',
                controller: 'ProfileCtrl',
                resolve: {
                    loginRequired: loginRequired
                }
            })
            .state('form', {
                url: '/form',
                templateUrl: 'form.html',
                controller: 'formController',
                redirectTo: 'form.search',
            })

            // nested states
            // each of these sections will have their own view
            // url will be nested (/form/search)
            .state('form.search', {
                url: '/search',
                templateUrl: 'form-search.html'

            })

            // url will be /form/register
            .state('form.registerflight', {
                url: '/registerfilght',
                templateUrl: 'form-registerflight.html',
                controller:'flightController'
            })
            // url will be /form/info
            .state('form.registerinfo', {
                url: '/registerinfo',
                templateUrl: 'form-registerinfo.html',
                controller:'infoController'
            })
            // url will be /form/payment
            .state('form.payment', {
                url: '/payment',
                templateUrl: 'form-payment.html',
                controller:'paymentController'
            });

        // catch all route
        // send users to the form page
        $urlRouterProvider.otherwise('/');
        $authProvider.facebook({
            //url: 'https://cheapflight-server-ndthinh.c9users.io/auth/facebook' ,
            url: 'http://localhost:3000/auth/facebook/' ,
            clientId: '1473462666027939',
          // redirectUri :'https://client-ndthinh.c9users.io/app/index.html#!/'
            redirectUri :'http://localhost:63342/'// chung nao up dc host thì chỉnh dòng này thành địa chỉ client // con2 gi2 nu7a hok
        });

        $authProvider.google({
            clientId: 'AIzaSyDM8o3UlTCiUfT8wMqjS3rfssve7v4DoXI'
        });
    })

    .directive('datefilter', function ($timeout) {

        return {
            require: '?ngModel',
            restrict: 'A',
            link: function (scope, element, attr,ngModel) {
                if(!ngModel) return; // do nothing if no ng-model

                ngModel.$render = function(){
                    element.find('input').val( ngModel.$viewValue || '' );
                }

                element.daterangepicker();

                element.on('dp.change', function(){
                    scope.$apply(read);
                });

                read();

                function read() {
                    var value = element.find('input').val();
                    ngModel.$setViewValue(value);
                }
                if (scope.$last === true &&scope.FlightType==="Multiple") {
                    $timeout(function () {
                        scope.$emit(attr.broadcastEventName ? attr.broadcastEventName : 'ngRepeatFinished');
                    });
                }
            }
        };
    })
    .run(['$rootScope','Idle', '$state','$uibModal', function($rootScope,Idle, $state, $uibModal) {

        $rootScope.$on('$stateChangeStart', function(evt, to, params) {
            if (to.redirectTo) {
                evt.preventDefault();
                $state.go(to.redirectTo, params, {location: 'replace'})
            }
        });
        $rootScope.$on('$stateChangeStart', function(event, toState, fromState){
            if (toState.name.indexOf("form") != -1 ) {
                Idle.watch();
            }else{
                Idle.unwatch();
            }
        });

        $rootScope.started = false;

        function closeModals() {
            if ($rootScope.warning) {
                $rootScope.warning.close();
                $rootScope.warning = null;
            }

            if ($rootScope.timedout) {
                $rootScope.timedout.close();
                $rootScope.timedout = null;
            }
        }

        $rootScope.$on('IdleStart', function() {
            closeModals();

            $rootScope.warning = $uibModal.open({
                templateUrl: 'warning-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.$on('IdleEnd', function() {
            closeModals();
        });

        $rootScope.$on('IdleTimeout', function() {
            closeModals();
            $rootScope.timedout = $uibModal.open({
                templateUrl: 'timedout-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.start = function() {
            closeModals();
            Idle.watch();
            $rootScope.started = true;
        };

        $rootScope.stop = function() {
            closeModals();
            Idle.unwatch();
            $rootScope.started = false;

        };
    }])

    .controller('formController',['$scope','$http','$state','apiFactory','$animate','$sanitize',function formController($scope, $http,$state,apiFactory) {
        $scope.choices = [{id: 'choice1'}];


        $scope.addNewChoice = function () {
            var newItemNo = $scope.choices.length + 1;
            $scope.choices.push({'id': 'choice' + newItemNo});


        };
        $scope.$on('ngRepeatFinished', function(){
                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: true,
                    singleDatePicker: true,
                    locale: {
                        cancelLabel: 'Clear'
                    },
                    showDropdowns: true
                });
           // }
            // else
            // {
            //     $('input[name="datefilter"]').daterangepicker()
            // }
        });
        $scope.removeChoice = function() {
            var lastItem = $scope.choices.length-1;
            $scope.choices.splice(lastItem);
            //console.log($scope.choices);
        };
        $scope.FlightType = undefined;
        $scope.currency = "VND";
        $scope.successful = "";
        $scope.adult = 1;
        $scope.children = 0;
        $scope.baby = 0;
        $scope.options = {
            adult: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            children: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            baby: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        };

        $scope.isMany = false;
        $scope.selected = undefined;
        $scope.$watch('FlightType', function () {
            $scope.isMany = false;
            $scope.choices.length=1;
            if ($scope.FlightType === "two") {
                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: true,
                    singleDatePicker: false,
                    showDropdowns: true
                });

            }
            else if ($scope.FlightType === "one") {
                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: true,
                    singleDatePicker: true,
                    locale: {
                        cancelLabel: 'Clear'
                    },
                    showDropdowns: true
                });
            }
            else if ($scope.FlightType === "Multiple") {
                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: true,
                    singleDatePicker: true,
                    locale: {
                        cancelLabel: 'Clear'
                    },
                    showDropdowns: true
                });
                $scope.isMany = true;

            }
        });

        //var ApiUrl='https://20161223t170649-dot-cheapflight-152614.appspot-preview.com';
        //var ApiUrl='https://20161217t081707-dot-cheapflight-152614.appspot-preview.com';
        //var ApiUrl='https://cheapflight-152614.appspot-preview.com';
        // var ApiUrl='http://localhost:8080';
        var ApiUrl = 'http://localhost:3000';
        //var ApiUrl = 'https://cheapflight-server-ndthinh.c9users.io';
        //var ApiUrl = 'http://192.168.1.94:3000';
        apiFactory.chuyenbay = [];
        $scope.getFlight = function () {

            if(apiFactory.formdata.length != 0) {
                if(apiFactory.formdata.flight.length > 1) {
                    $scope.FlightType = "Multiple";
                }
                else if(apiFactory.formdata.endtime)
                    $scope.FlightType = "two";
                else
                    $scope.FlightType = "one";
                $scope.choices = apiFactory.formdata.flight;
                $scope.adult = apiFactory.formdata.nguoilon;
                $scope.children =apiFactory.formdata.treem;
               $scope.baby = apiFactory.formdata.baby;
            }
            else
            {
                $scope.FlightType = "two";
            }
            $http.get(ApiUrl + '/api/chuyenbay/sanbaydi').then(function (response) {
                $scope.flightsdepart = response.data;
            });
        };

        $scope.getFlightback = function (flight) {
            if (!flight) {
                $http.get(ApiUrl + '/api/chuyenbay/sanbaydi').then(function (response) {
                    $scope.flightsback = response.data;
                    console.log(response);
                });
            }
            else {
                var url = ApiUrl + '/api/chuyenbay/sanbayden?sanbaydi=' + flight.MaSanBay;
                console.log(url);
                $http.get(url).then(function (response) {
                    $scope.flightsback = response.data;
                    console.log(response);
                });
            }
        };

        $scope.searchOneFlight = function (start, end,startdate, numadult, numchildren, numbaby, current) {
            var url = ApiUrl + '/api/chuyenbay';
            startdate = Date.parse(startdate) / 1000;
            console.log(startdate);
            $http.get(url, {
                params: {
                    "sanbaydi": start, "sanbayden": end, "nguoilon": numadult,
                    "treem": numchildren, "tresosinh": numbaby, "tiente": current,
                    "ngaydi": startdate
                }
            }).then(function (response) {
                $scope.status = response.status;
                $scope.successful = response.data;
                var flight = response.data;
                console.log(flight);
                for (var i = 0; i < response.data.length; i++) {
                    var date = new Date(response.data[i].ThoiGianDi * 1000);
                    var hours = date.getHours();
                    var minutes = "0" + date.getMinutes();
                    var formattedTime = hours + ':' + minutes.substr(-2);
                    flight[i].GioBay = formattedTime;
                    flight[i].SoGhe = response.data[i].SoLuongGhe - response.data[i].GheDaDat;
                }
                console.log(flight);
                apiFactory.formdata=({'flight':$scope.choices,'starttime':startdate,'nguoilon':numadult,'treem':numchildren,'baby':numbaby})

                apiFactory.chuyenbay.push(flight);
                $state.go('form.registerflight');

            }, function (response) {
                $scope.err = response.data || 'Request failed';
                $scope.status = response.status;
                console.log($scope.err + ":" + $scope.status);
            });
        };

        $scope.searchFlight = function (start, end,startdate,enddate, numadult, numchildren, numbaby, current, first) {
            var url = ApiUrl + '/api/chuyenbay';
            var date = "2016/12/31";
            startdate = Date.parse(startdate) / 1000;
            enddate = Date.parse(enddate) / 1000;
            console.log(startdate);
            $http.get(url, {
                params: {
                    "sanbaydi": start, "sanbayden": end, "nguoilon": numadult,
                    "treem": numchildren, "tresosinh": numbaby, "uutien": first, "tiente": current,
                    "ngaydi": startdate
                }
            }).then(function (response) {
                $scope.status = response.status;
                $scope.successful = response.data;
                var flight = response.data;
                console.log(flight);
                for (var i = 0; i < response.data.length; i++) {
                    var date = new Date(response.data[i].ThoiGianDi * 1000);
                    var hours = date.getHours();
                    var minutes = "0" + date.getMinutes();
                    var formattedTime = hours + ':' + minutes.substr(-2);
                    flight[i].GioBay = formattedTime;
                    flight[i].SoGhe = response.data[i].SoLuongGhe - response.data[i].GheDaDat;
                }
                console.log(flight);
                apiFactory.chuyenbay.push(flight);

            }, function (response) {
                $scope.err = response.data || 'Request failed';
                $scope.status = response.status;
                console.log($scope.err + ":" + $scope.status);
                return;
            });
            if($scope.FlightType=="two") {
                $http.get(url, {
                    params: {
                        "sanbaydi": end, "sanbayden": start, "nguoilon": numadult,
                        "treem": numchildren, "tresosinh": numbaby, "uutien": first, "tiente": current,
                        "ngaydi": enddate
                    }
                }).then(function (response) {
                    $scope.status = response.status;
                    $scope.successful = response.data;
                    var flightback = response.data;
                    for (var i = 0; i < response.data.length; i++) {
                        var date = new Date(response.data[i].ThoiGianDi * 1000);
                        var hours = date.getHours();
                        var minutes = "0" + date.getMinutes();
                        var formattedTime = hours + ':' + minutes.substr(-2);
                        flightback[i].GioBay = formattedTime;
                        flightback[i].SoGhe = response.data[i].SoLuongGhe - response.data[i].GheDaDat;
                    }
                    console.log(flightback);
                    apiFactory.chuyenbay.push(flightback);
                    apiFactory.formdata=({'flight':[{'arrival':$scope.choices[0].arrival,'flightdepart':$scope.choices[0].flightdepart},
                                                        {'arrival':$scope.choices[0].flightdepart,'flightdepart':$scope.choices[0].arrival}]
                        ,'starttime':startdate,'endtime':enddate,'nguoilon':numadult,'treem':numchildren,'baby':numbaby});
                    $state.go('form.registerflight');

                }, function (response) {
                    $scope.err = response.data || 'Request failed';
                    $scope.status = response.status;
                    console.log($scope.err + ":" + $scope.status);
                    return;

                });
            }

        };
        $scope.searchFlightMany = function (start, end,startdate, numadult, numchildren, numbaby, current) {
            var url = ApiUrl + '/api/chuyenbay/nhieuchang';
            var changbay = {};
            changbay["changbay"]=[];
            for(var i=0; i<start.length;i++){
                changbay["changbay"].push({'sanbaydi': start[i],'sanbayden':end[i],'thoigiandi': Date.parse(startdate[i])/1000});
            }
            var req = {
                method: 'POST',
                url: url,
                data: ({
                    'changbay':changbay["changbay"],
                    'nguoilon':numadult,
                    'treem' : numchildren,
                    'sosinh' : numbaby,
                    'tiente' :current
                })
                ,
                headers:  { 'Content-Type': 'application/json;charset=utf-8' },

            };
            $http(req).then(function(response) {
                $scope.status = response.status;
                $scope.successful = response.data;

                apiFactory.chuyenbay.length=0;
                var flight = response.data;
                for (var i = 0; i < response.data.length; i++) {
                    for (var j = 0; j < response.data[i].length; j++) {

                        var date = new Date(response.data[i][j].ThoiGianDi * 1000);
                        var hours = date.getHours();
                        var minutes = "0" + date.getMinutes();
                        var formattedTime = hours + ':' + minutes.substr(-2);
                        flight[i][j].GioBay = formattedTime;
                        flight[i][j].SoGhe = response.data[i][j].SoLuongGhe - response.data[i][j].GheDaDat;
                    }
                    apiFactory.formdata=({'flight':$scope.choices,'starttime':startdate,'nguoilon':numadult,'treem':numchildren,'baby':numbaby});

                    apiFactory.chuyenbay.push(flight[i]);

                }

                console.log(flight);

                $state.go('form.registerflight');
            });

        };
        $scope.SaveInfo = function () {
            // if ($scope.customSelected1 == null || $scope.customSelected2 == null)
            // {
            //     alert('Bạn cần điền đầy đủ thông tin');
            // }
            // else {
            console.log( $scope.choices);
                var numberadult = $scope.adult;
                var numberchildren = $scope.children;
                var numberbaby = $scope.baby;
                if ((numberadult / 2) <= numberchildren && (numberadult / 2) <= numberbaby) {
                    alert('Số trẻ em vượt quá số lượng cho phép')
                }
                else {
                    var start =[];
                    var end =[];
                    var startdate=[];
                    var enddate=[];
                    console.log( $scope.choices[0].flightdepart.MaSanBay);
                    for(var i = 0; i < $scope.choices.length; i++) {
                       console.log( $scope.choices[i].flightdepart.MaSanBay);
                        start.push($scope.choices[i].flightdepart.MaSanBay);
                        end.push($scope.choices[i].arrival.MaSanBay);
                        var arr = $scope.choices[i].date.split(' - ');
                        startdate.push(arr[0]);
                        if($scope.FlightType == "two")
                            enddate.push(arr[1]);
                    }
                    console.log(startdate);
                    var numadult = $scope.adult;
                    var numchildren = $scope.children;
                    var numbaby = $scope.baby;
                    var current = $scope.currency;
                    if($scope.FlightType==="Multiple")
                        $scope.searchFlightMany(start, end, startdate, numadult, numchildren, numbaby, current);
                    else  if($scope.FlightType==="two")
                        $scope.searchFlight(start[0], end[0], startdate[0], enddate[0], numadult, numchildren, numbaby, current);
                    else  if($scope.FlightType==="one")
                        $scope.searchOneFlight(start[0], end[0], startdate[0], numadult, numchildren, numbaby, current);
                }
            //}

        };
        angular.element(document).ready(function () {
            // Set focus
            $('input[name="datefilter"]').daterangepicker();
        });

    }])
    .controller()
    .factory('apiFactory', function ($rootScope, $q, $window) {

        var chuyenbay = [];
        var formdata=[];
        function setData(flight) {
            chuyenbay.push(flight);
        }

        return {
            chuyenbay: chuyenbay,
            formdata:formdata,
            setData: setData
        };
    });


