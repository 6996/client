angular.module('myApp')
  .factory('Account', function($http) {
    return {
      getProfile: function() {
       // return $http.get('https://cheapflight-server-ndthinh.c9users.io/api/me');
         return $http.get('http://localhost:3000/api/me');
      },
      updateProfile: function(profileData) {
       // return $http.put('https://cheapflight-server-ndthinh.c9users.io/api/me', profileData);
         return $http.put('http://localhost:3000/api/me', profileData);
      }
    };
  });