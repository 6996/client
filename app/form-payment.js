/**
 * Created by bathi on 07-Jan-17.
 */
var app = angular.module('myApp')
    .controller('paymentController', ['$scope','$http','$state','apiFactory','AlertService','$animate','$sanitize',
        function paymentController( $scope, $http,$state, apiFactory,AlertService) {
        $scope.results=[];

        $scope.init1=function () {

            if(apiFactory.formdata && apiFactory.chuyenbay)
            {
                console.log(apiFactory.formdata);
                console.log(apiFactory.chuyenbay);
                $scope.TongTien=0;
                $scope.MaDatCho=apiFactory.chuyenbay.MaDatCho;
                for(var i=0;i<apiFactory.chuyenbay.changbay.length;i++)
                {
                    var thue=0;var thuetreem=0;
                    var adultprice = parseFloat(apiFactory.chuyenbay.changbay[i][0].GiaBan);
                    var childprice = adultprice / 2;
                    if(apiFactory.formdata.nguoilon > 0)
                        thue =140000;
                   if(apiFactory.formdata.treem>0)
                       thuetreem=140000;
                    var thoigiandi=new Date(apiFactory.chuyenbay.changbay[i][0].Ngay*1000);
                    $scope.results.push({
                        MaChuyenBay: apiFactory.chuyenbay.changbay[i][0].MaChuyenBay,
                        ThoiGianDi : thoigiandi.toDateString() + ' '+thoigiandi.toTimeString(),
                        adultprice : adultprice ,
                        childprice : childprice,
                        adult : apiFactory.formdata.nguoilon,
                        thue:thue,
                        adultmoney : apiFactory.formdata.nguoilon * (adultprice + thue),
                        child : apiFactory.formdata.treem,
                        thuetreem:thuetreem,
                        childmoney : apiFactory.formdata.treem * (childprice + thuetreem),
                        flightdepart:'',
                        arrival:''
                    });

                    for(var j = 0; j < apiFactory.formdata.flight.length; j++){
                        if(apiFactory.chuyenbay.changbay[i][0].MaChuyenBay===apiFactory.formdata.flight[j].MaChuyenBay)
                        {
                            $scope.results[i].flightdepart = apiFactory.formdata.flight[j].flightdepart;
                            $scope.results[i].arrival = apiFactory.formdata.flight[j].arrival;
                            break;
                        }
                    }
                    $scope.TongTien += $scope.results[i].adultmoney + $scope.results[i].childmoney;
                }

                $scope.thue=140000;
                $scope.fullname = apiFactory.chuyenbay.LienHe[0].Ho + apiFactory.chuyenbay.LienHe[0].Ten;
                $scope.phone = apiFactory.chuyenbay.LienHe[0].SoDienThoai;
                $scope.emailcontact = apiFactory.chuyenbay.LienHe[0].Email;
                console.log( $scope.results);
            }
            else
            {
            }
        };
        $scope.init1();
        $scope.back =function () {
            $state.go('home');
        };
//var ApiUrl='https://20161223t170649-dot-cheapflight-152614.appspot-preview.com';
            //var ApiUrl='http://localhost:8080';
            //var ApiUrl='http://192.168.2.12:3000';
            var ApiUrl='http://localhost:3000';
           // var ApiUrl = 'https://cheapflight-server-ndthinh.c9users.io';
        $scope.submit = function () {
            var url = ApiUrl + '/api/datcho/'+apiFactory.chuyenbay.MaDatCho ;
            var req = {
                method: 'PUT',
                url: url,
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: ({
                    'madatcho':apiFactory.chuyenbay.MaDatCho ,
                    'TongTien':$scope.TongTien
                })
            };
            console.log(req.data);
            $http(req).then(function (response) {
                $scope.status = response.status;
                $scope.successful = response.data;
                AlertService.alert('Chúc mừng', 'Bạn đã đặt chỗ thành công', 'Quay về trang chủ', $scope.back, 'Hủy bỏ');


            });
        }
    }]);