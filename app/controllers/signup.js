angular.module('myApp')
  .controller('SignupCtrl', function($scope, $http,$location, $auth, toastr) {
    $scope.signup = function() {
        $http.post('http://localhost:3000/auth/signup', $scope.user)
        //$http.post('https://cheapflight-server-ndthinh.c9users.io/auth/signup', $scope.user)
        .then(function(response) {
          $auth.setToken(response);
          $location.path('/');
          toastr.info('You have successfully created a new account and have been signed-in');
        })
        .catch(function(response) {
          toastr.error(response.data.message);
        });
    };
  });