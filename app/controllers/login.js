angular.module('myApp')
  .controller('LoginCtrl', function($scope,$http, $rootScope, Account,$location, $auth, toastr) {
    $scope.login = function() {
        $http.post('http://localhost:3000/auth/login', $scope.user)
        //$http.post('https://cheapflight-server-ndthinh.c9users.io/auth/login', $scope.user)
        .then(function() {
          toastr.success('You have successfully signed in!');
          $location.path('/');
        })
        .catch(function(error) {
          toastr.error(error.data.message, error.status);
        });
    };
    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          toastr.success('You have successfully signed in with ' + provider + '!');
          $location.path('/');
        })
        .catch(function(error) {
          if (error.message) {
            // Satellizer promise reject error.
            toastr.error(error.message);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
    };
  });
