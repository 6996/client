/**
 * Created by bathi on 02-Jan-17.
 */
var app = angular.module('myApp')
    .controller('infoController', ['$scope','$http','$state','apiFactory','$animate','$sanitize', function infoController( $scope, $http, $state, apiFactory) {
        $scope.callscontact = '';
        $scope.options = {
            calls: ['Ông','Bà','Anh','Chị'],
            callscontact: ['Ông','Bà','Anh','Chị']
        };
        $(function() {
            $('input[name="birthday"]').daterangepicker({
                autoUpdateInput: true,
                singleDatePicker: true,
                locale: {
                    cancelLabel: 'Clear'
                },
                showDropdowns: true
            });
        });
        $scope.numpassenger=[];
        $scope.init=function () {
            if(apiFactory.formdata.length != 0)
            {
                $scope.numpassenger.length = 0;
                var numPass = apiFactory.formdata.nguoilon + apiFactory.formdata.treem;
                for(var i=0;i<numPass;i++)
                {
                    $scope.numpassenger.push({"id":i});
                }
            }
            else
            {
                $scope.numpassenger =[{
                    calls :'',
                    familyname :'',
                    name :''
                }];
            }
        };
        $scope.init();
        $scope.addFormPass = function () {
            $scope.numpassenger.push({});
            apiFactory.formdata.nguoilon++;
        };
        $scope.removeFormPass = function (item) {
            if($scope.numpassenger.length>1) {
                $scope.numpassenger.splice(item);
                apiFactory.formdata.nguoilon--;
            }
            else
                alert("Số khách hàng phải lớn hơn 1");
        };
        //var ApiUrl='https://20161223t170649-dot-cheapflight-152614.appspot-preview.com';
        //var ApiUrl='http://localhost:8080';
        //var ApiUrl='http://192.168.2.12:3000';
        var ApiUrl='http://localhost:3000';
        //var ApiUrl = 'https://cheapflight-server-ndthinh.c9users.io';
        $scope.submit = function () {
            if (  $scope.callscontact == '' || $scope.calls == '')
            {
                alert('Vui lòng nhập đầy đủ thông tin')
            }
            console.log(apiFactory.chuyenbay.MaDatCho);
            var url = ApiUrl + '/api/chitietchuyenbay/' + apiFactory.chuyenbay.MaDatCho + "/HanhKhach";
            var HanhKhach = {};
            HanhKhach["HanhKhach"]=[];
            for(var i=0; i < $scope.numpassenger.length;i++){
                HanhKhach["HanhKhach"].push({
                    'DanhXung': $scope.numpassenger[i].calls,
                    'Ho':$scope.numpassenger[i].familyname,
                    'Ten' : $scope.numpassenger[i].name,
                    'NgaySinh' : Date.parse($scope.numpassenger[i].birthday)/1000
                });
            }
            var LienHe=[];
                LienHe.push({
                    'DanhXung': $scope.callscontact,
                    'Ho':$scope.familynamecontact,
                    'Ten' : $scope.namecontact,
                    'Email' : $scope.mailcontact,
                    'SoDienThoai' :$scope.phonecontact
                });
            console.log(LienHe);
            // use $.param jQuery function to serialize data from JSON

            var req = {
                method: 'POST',
                url: url,
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: ({
                    'HanhKhach':HanhKhach["HanhKhach"],
                    'LienHe':LienHe
                })
            };
            console.log(req.data);
            $http(req).then(function (response) {
                $scope.status = response.status;
                $scope.successful = response.data;
                var flight = response.data;
                apiFactory.chuyenbay.HanhKhach = req.data.HanhKhach;

                apiFactory.chuyenbay.LienHe = req.data.LienHe;
                $state.go('form.payment');

            });

        }
    }]);