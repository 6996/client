
/**
 * Created by bathi on 30-Dec-16.
 */
var app = angular.module('myApp')
.service('AlertService', function($uibModal){
    /*
     headerText - presents text in header
     bodyText - presents text in body
     buttonText - presents text in button. On its click if method parameters is not passed, modal will be closed.
     In situation that the method parameters is passed, on its click, method will be called. For situations
     like that, there is parameter buttonText2 which will be used as cancel modal functionality.
     method - presents passed function which will be called on confirmation
     buttonText2 - presents text in button for cancel

     */
    var alert = function(headerText, bodyText, buttonText, method, buttonText2){

        method = method || function(){};
        buttonText2 = buttonText2 || '';

        $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent',
            controller: 'AlertModalInstanceCtrl',
            size: 'md',
            resolve: {
                headerText: function () {
                    return headerText;
                },
                bodyText: function () {
                    return bodyText;
                },
                buttonText: function () {
                    return buttonText;
                },
                method: function () {
                    return method;
                },
                buttonText2: function () {
                    return buttonText2;
                }
            }
        });
    };

    return{
        alert: alert
    };

}).controller('AlertModalInstanceCtrl', function ($scope, $uibModalInstance, headerText, bodyText, buttonText, method, buttonText2) {
    $scope.headerText = headerText;
    $scope.bodyText = bodyText;
    $scope.buttonText = buttonText;
    $scope.method = method;
    $scope.buttonText2 = buttonText2;

    $scope.ok = function () {
        $scope.method();
        $uibModalInstance.dismiss('cancel');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})
    .controller('flightController', ['$scope','$http','$state','AlertService','apiFactory','$animate','$sanitize', function flightController($scope,$http,$state,AlertService,apiFactory) {
        $scope.results = [];
        $scope.selectedFlight = [];
    $scope.listicket = [];
    $scope.isTwoWay=false;
        $scope.nlinks = [];
        function RutTrich() {
            var url = "http://localhost:3000/api/chuyenbay/ruttrich";
            //var url = "https://cheapflight-server-ndthinh.c9users.io/api/chuyenbay/ruttrich";
            for(var j = 0; j < apiFactory.formdata.flight.length; j++){
                $http.get(url, {
                    params: {
                        "sanbaydi": apiFactory.formdata.flight[j].flightdepart.MaSanBay, "sanbayden": apiFactory.formdata.flight[j].arrival.MaSanBay, "nguoilon": apiFactory.formdata.flight[j].nguoilon,
                        "treem": apiFactory.formdata.flight[j].treem, "sosinh": apiFactory.formdata.flight[j].baby, "uutien": "1", "tiente": apiFactory.formdata.flight[j].tiente,
                        "ngaydi": Date.parse(apiFactory.formdata.flight[j].date)/1000, "hang":"Y"
                    }})
                    .then(function (response) {
                    console.log(response.data);
                    for (var k = 0; k< 5; k++)
                    {
                        $scope.nlinks.push(response.data[k]);
                    }

                    console.log($scope.nlinks);
                })
            }
        }
        RutTrich();

    $scope.getData = function() {
        for (var i = 0; i < apiFactory.chuyenbay.length; i++) {
            $scope.results.push({'id': i, 'flights': apiFactory.chuyenbay[i]});
        }
        console.log($scope.results);
    };
        $scope.getData();
    $scope.chonChuyenBay = function (id, item) {
            $scope.selectedFlight[id] = item;
        };
    $scope.registerFlight = function () {
            var url = ApiUrl + '/api/datcho/nhieuchang';
            console.log($scope.selectedFlight);
            var changbay = {};
            changbay["changbay"]=[];
            for(var i=0; i < $scope.selectedFlight.length;i++){
                if($scope.selectedFlight[i]) {
                    apiFactory.formdata.flight[i].MaChuyenBay = $scope.selectedFlight[i].Ma;
                    changbay["changbay"].push({
                        'MaChuyenBay': $scope.selectedFlight[i].Ma,
                        'ThoiGian': $scope.selectedFlight[i].ThoiGianDi.toString(),
                        'Hang': $scope.selectedFlight[i].Hang,
                        'MucGia': $scope.selectedFlight[i].MucGia,
                        'GiaBan': $scope.selectedFlight[i].GiaBan
                    });
                }
            }
            // use $.param jQuery function to serialize data from JSON

            var req = {
                method: 'POST',
                url: url,
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: ({
                    'changbay':changbay["changbay"]
                })
            };
        console.log(req.data);
            $http(req).then(function (response) {
                $scope.status = response.status;
                $scope.successful = response.data;
                var flight = response.data;

                var madatcho = (flight[0][0].MaDatCho);
                apiFactory.chuyenbay.length = 0;
                apiFactory.chuyenbay = ({'changbay':flight,'MaDatCho':madatcho});
                console.log(apiFactory.chuyenbay);
                $state.go('form.registerinfo');
            });
        };

    //var ApiUrl='https://20161223t170649-dot-cheapflight-152614.appspot-preview.com';
    //var ApiUrl='http://localhost:8080';
    //var ApiUrl='http://192.168.2.12:3000';
    var ApiUrl='http://localhost:3000';
       // var ApiUrl = 'https://cheapflight-server-ndthinh.c9users.io';
    $scope.submit = function() {
        if( $scope.selectedFlight.length < $scope.results.length)
            AlertService.alert('Bạn có muốn tiếp tục?', 'Số chuyên bay bạn chọn ít hơn số chuyến bay khi bạn tìm kiếm', 'Tiếp tục', $scope.registerFlight, 'Hủy bỏ');
        else
            $scope.registerFlight();
        };

}])